# oss-llms

## Using Whisper.cpp

convert a directory full of videos to WAV files
```bash
cvt_videos () {
	for f in "$1"/*.mp4
	do
		ffmpeg -i "$f" -ar 16000 -ac 1 -c:a pcm_s16le "$1/WAV/${f##*/}.wav"
	done
}
```
transcribe a directory full of WAV files
```bash
transcribe_audio_dir () {
	find "$1" -name "*.wav" -print0 | while IFS= read -r -d '' f
	do
		echo "Transcribing $f"
		./main -m models/ggml-base.en.bin -f "$f" > "${f%.*}.txt"
	done
}

```
combine transcripts
```bash
#!/bin/bash

# Set transcripts folder path
transcripts_dir="$1"

# Output file 
output_file="$2"

# Loop through all .txt files
for txt_file in "$transcripts_dir"/*.txt; do

  # Append file content 
  cat "$txt_file" >> "$output_file"

  # Add new line  
  echo >> "$output_file"

done

echo "Concatenated transcript files from $transcripts_dir into $output_file"


```