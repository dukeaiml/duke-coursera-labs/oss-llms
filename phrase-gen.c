//bin/cosmocc -o phrase-gen phrase-gen.c
//./phrase-gen -c 5 -p "Hello there"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

// Repeat phrase N times
void phrase_generator(int count, char* phrase) {
  for (int i = 0; i < count; i++) {
    printf("%s\n", phrase);
  }
}

int main(int argc, char* argv[]) {

  // Default values
  int count = 1;
  char* phrase = "Hello world";

  // Parse command line arguments
  for (int i = 1; i < argc; i++) {
    if (strcmp(argv[i],"--count") == 0 || strcmp(argv[i],"-c") == 0) {
      count = atoi(argv[i+1]);
    }
    else if (strcmp(argv[i],"--phrase") == 0 || strcmp(argv[i],"-p") == 0) {
      phrase = argv[i+1];
    }
  }

  // Generate phrases
  phrase_generator(count, phrase);

  return 0;
}